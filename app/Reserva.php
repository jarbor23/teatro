<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reserva extends Model
{
    //
    protected $table = 'reservas';

    protected $primaryKey = 'id';

    protected $fillable = ['fecha_reserva', 'usuario_id', 'numero_persona'];

}
